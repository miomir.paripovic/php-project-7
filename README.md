# Project no. 7


## Task

This is my assignment for week no. 7. The main goal for the project is to create a web page (index.php) which contains a form with basic data to enter: first and last name, day of birth, email, etc. Send all data to 'register.php' by POST method. 'register.php' does validation and returns an error in case that input data is not valid. If data is entered correctly, 'register.php' prints all the information. Date of birth has to contain the day of the week for that specific day.