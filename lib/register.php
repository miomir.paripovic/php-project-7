<?php
function checkEmailAdress($data) {
    if (strpos($data, '@gmail.com') === false)
        return true;
    else
        return false;
}

function checkYear($data) {
    if ((int) substr($data, 0, 4) < 1920) {
        return true;
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// define variables and set to empty values
$firstnameErr = $lastnameErr = $emailErr = $genderErr = $dateofbirthErr = "";
$firstname    = $lastname = $email = $gender = $dateofbirth = $params = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    if (empty($_POST["firstname"])) {
        $firstnameErr = "First name is required";
    } else {
        $firstname = test_input($_POST["firstname"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $firstname)) {
            $firstnameErr = "Only letters and white space allowed";
        } elseif (strlen($firstname) < 2) {
            $firstnameErr = "Minimum two characters";
        }
    }
    
    if (empty($_POST["lastname"])) {
        $lastnameErr = "Last name is required";
    } else {
        $lastname = test_input($_POST["lastname"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/", $lastname)) {
            $lastnameErr = "Only letters and white space allowed";
        } elseif (strlen($lastname) < 2) {
            $lastnameErr = "Minimum two characters";
        }
    }
    
    if (empty($_POST["email"])) {
        $emailErr = "Email is required";
    } else {
        $email = test_input($_POST["email"]);
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        } elseif (checkEmailAdress($email)) {
            $emailErr = "Not a valid gmail address";
        } elseif (strlen($email) < 14) {
            $emailErr = "Not enough characters";
        }
    }
    
    if (empty($_POST["dateofbirth"])) {
        $dateofbirthErr = "Date of birth is required";
    } else {
        $dateofbirth = $_POST["dateofbirth"];
        
        if (checkYear($dateofbirth)) {
            $dateofbirthErr = "If you're this old, you're probably already dead";
        }
    }
    
    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
    } else {
        $gender = $_POST["gender"];
    }
    
    if (!empty($firstnameErr) or !empty($lastnameErr) or !empty($emailErr) or !empty($genderErr) or !empty($dateofbirthErr)) {
        $params .= "&firstname=" . urlencode($_POST["firstname"]);
        $params .= "&lastname=" . urlencode($_POST["lastname"]);
        $params .= "&email=" . urlencode($_POST["email"]);
        $params .= "&gender=" . urlencode($gender);
        $params .= "&dateofbirth=" . urlencode($_POST["dateofbirth"]);
        
        $params .= "&firstnameErr=" . urlencode($firstnameErr);
        $params .= "&lastnameErr=" . urlencode($lastnameErr);
        $params .= "&emailErr=" . urlencode($emailErr);
        $params .= "&genderErr=" . urlencode($genderErr);
        $params .= "&dateofbirthErr=" . urlencode($dateofbirthErr);
        
        header("Location: ../index.php?" . $params);
        exit;
    }
}
?>

<!DOCTYPE HTML>  
<html>
    
<head>
    <title>Register page</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">  
    <link rel="icon" href="../images/favicon.png">
</head>
    
<body>      
    <div class="wrapper">
        <header class="header">
            <img  class="header__logo-image" src="../images/phplogo.png" alt="Logo image" title="Logo image">            
        </header>        
        <!-- End of header -->
        <div class="main">
        <?php
             if (empty($firstnameErr) or empty($lastnameErr) or empty($emailErr) or empty($genderErr) or empty($dateofbirthErr))
                {
                    echo '<div class="output">';
                    echo "<h2>Your Input:</h2>";
                    echo "First Name: " . $_POST['firstname'];
                    echo "<br>";

                    echo "Last Name: " . $_POST['lastname'];
                    echo "<br>";

                    echo "Email: " . $_POST['email'];
                    echo "<br>";        

                    echo 'Your date of birth is: ';
                    $date = date_create($dateofbirth);
                    echo date_format($date,"l, F j, Y");
                    echo "<br>";
                    echo "<br>";

                    echo "Gender: " . $_POST['gender'];  
                    echo "<br>";
                                        
                    echo "Your coding level: " . $_POST['codinglevel'];  
                    echo "<br>";

                    echo "Your favorite development tool: " . $_POST['favorite'];  
                    echo "<br>";
                    
                    if (!empty($_POST['fav-lang'])) {
                        echo "Your favorite programming language(s):";
                        echo '<ul class="list">';
                        foreach($_POST['fav-lang'] as $selected) {
                            echo '<li>' . $selected . "</li>";
                        }
                        echo '</ul>';
                    }
                    echo "<br>";                      
                    echo "<a href=\"../index.php\">Return to form</a>";        
                    echo '</div>';
              }                          
        ?>
        </div>
    </div> 
</body>
    
</html>