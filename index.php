<?php
    $firstname = $lastname = $email = $gender = $dateofbirth= "";
    $firstnameErr = $lastnameErr = $emailErr = $genderErr = $dateofbirthErr = "";
    
    if (isset($_GET['firstname'])) { $firstname = $_GET['firstname']; }
    if (isset($_GET['lastname'])) { $lastname = $_GET['lastname']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['dateofbirth'])) { $dateofbirth = $_GET['dateofbirth']; }
    if (isset($_GET['gender'])) { $gender = $_GET['gender']; }
    
    if (isset($_GET['firstnameErr'])) { $firstnameErr = $_GET['firstnameErr']; }
    if (isset($_GET['lastnameErr'])) { $lastnameErr = $_GET['lastnameErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['dateofbirthErr'])) { $dateofbirthErr = $_GET['dateofbirthErr']; }    
    if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }    
?>

<!DOCTYPE HTML>  

<html>

<head>
    <title>Project no. 7</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"> 
    <link rel="icon" href="images/favicon.png">
</head>

<body>  
    <div class="wrapper">
        <header class="header">
            <img  class="header__logo-image" src="images/phplogo.png" alt="Logo image" title="Logo image">            
        </header>        
        <!-- End of header -->
        <div class="main">             
             <form action="lib/register.php" method="post">
                <fieldset class="fieldset">
                    <legend class="legend">Basic Info</legend>
                    <label for="firstname">First Name <span class="required">*</span><span class="error"><?php echo $firstnameErr; ?></span></label>
                    <input type="text" name="firstname" class="std-input" placeholder="Enter your name" value="<?php echo $firstname; ?>">
                    <label for="lastname">Last name <span class="required">*</span><span class="error"><?php echo $lastnameErr; ?></span></label>
                    <input type="text" name="lastname" class="std-input" placeholder="Enter your last name" value="<?php echo $lastname; ?>">
                    <label for="dateofbirth">Date of Birth <span class="required">*</span><span class="error"><?php echo $dateofbirthErr; ?></span></label>
                    <input type="date" name="dateofbirth" class="std-input" value="<?php echo $dateofbirth; ?>"> 
                    <label for="email">Email <span class="required">*</span><span class="error"><?php echo $emailErr; ?></span></label>
                    <input type="email" name="email" class="std-input" placeholder="john.doe@gmail.com" value="<?php echo $email; ?>"> 
                    <label>Gender: <span class="required">*</span><span class="error" value="<?php echo $firstname; ?>"><?php echo $genderErr; ?></span></label><br>
                    <label><input class="radio" type="radio" name="gender" value="male" <?php echo ($gender == 'male' ? 'checked="checked"' : ''); ?>>Male</label><br>
                    <label><input class="radio" type="radio" name="gender" value="female" <?php echo ($gender == 'female' ? 'checked="checked"' : ''); ?>>Female</label><br>
                    <label><input class="radio" type="radio" name="gender" value="other" <?php echo ($gender == 'other' ? 'checked="checked"' : ''); ?>>Other</label><br>
                </fieldset>
                <fieldset class="fieldset">
                    <legend>Choose yor coding level</legend>                                        
                    <select class="codinglevel" name="codinglevel">
                        <option value="none">none</option>
                        <option value="noob">noob</option>
                        <option value="geek">geek</option>
                        <option value="guru">guru</option>
                        <option value="godlike">godlike</option>
                    </select><br><br>
                    <label>Which is your favorite?</label><br>            
                    <label><input class="radio" type="radio" name="favorite" value="none" checked="checked">None</label><br>
                    <label><input class="radio" type="radio" name="favorite" value="HTML">HTML</label><br>
                    <label><input class="radio" type="radio" name="favorite" value="CSS">CSS</label><br>
                    <label><input class="radio" type="radio" name="favorite" value="JavaScript">JavaScript</label><br>
                    <label><input class="radio" type="radio" name="favorite" value="All of the Above">All of the Above</label>
                </fieldset>
                <fieldset class="fieldset">
                    <legend>What is your favorite programming language?</legend>             
                    <label><input class="check-box" type="checkbox" name="fav-lang[]" value="JavaScipt">JavaScript</label><br>
                    <label><input class="check-box" type="checkbox" name="fav-lang[]" value="C/C++/C#">C/C++/C#</label><br>
                    <label><input class="check-box" type="checkbox" name="fav-lang[]" value="Java">Java</label><br>
                    <label><input class="check-box" type="checkbox" name="fav-lang[]" value="Python">Python</label><br>
                    <label><input class="check-box" type="checkbox" name="fav-lang[]" value="Ruby">Ruby</label>
                </fieldset>                            
                    <input class="submit-button" type="submit" name="submit" value="SUBMIT">                    
            </form>  
        </div>
    </div> 

</body>

</html>